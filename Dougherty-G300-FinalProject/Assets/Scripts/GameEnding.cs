using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnding : MonoBehaviour
{

    public GameObject gameEnd;
    public GameObject door;

    public GameObject endScore;

    public PlayerController controller;

    public Pickups key;

    public PointsController savedPoints;

    public GameObject playerDied;
    public EnemyHit Health;
    public GameObject instructBackground;


    // Start is called before the first frame update
    void Start()
    {
        gameEnd.SetActive(false);
        playerDied.SetActive(false);
        savedPoints.npcSaved.SetActive(false);
    }

    void Awake()
    {
        door.GetComponent<Collider2D>().isTrigger = true;
    }


    void OnTriggerEnter2D(Collider2D door)
    {
        if (door.CompareTag("Player") && key.keyCount ==1 )
        {

            controller.instructBackground.SetActive(true);
            gameEnd.SetActive(true);

            Time.timeScale = 0f;

            savedPoints.npcSaved.SetActive(true);

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Health.health == 0)
        {
            playerDied.SetActive(true);
            instructBackground.SetActive(true);
            Time.timeScale = 0f;
        }
    }


}
