using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHit : MonoBehaviour
{
    public static EnemyHit instance;
    public GameObject enemy;
    public Rigidbody2D knockBack;

    //public bool isKnockbacked = false;

    public PlayerController Player;
    public float health = 10;
    public Text healthText;

    public GameObject fullHealth;
    public GameObject halfHealth;
    public GameObject noHealth;


    public IEnumerator Knockback(float knockbackDuration, float knockbackPower, Transform obj)
    {
        float timer = 0;

        while (knockbackDuration > timer)
        {
            //isKnockbacked = true;

            timer += Time.deltaTime;
            Vector2 direction = (obj.transform.position - this.transform.position).normalized;
            knockBack.AddForce(-direction * knockbackPower);

            yield return 0;

            //isKnockbacked = false;

        }
        

        
    }

    private void Start()
    {
        healthText.text = health.ToString();

        fullHealth.SetActive(true);
        halfHealth.SetActive(false);
        noHealth.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D enemy)
    {
        if (enemy.CompareTag("Enemy") && Player.isDashing == false)
        {
            health = health - 1;
            
            if (health <= 0)
            {
                
            }
            else
            {
                healthText.text = health.ToString();
                
            }
            
            if (health > 5)
            {
                fullHealth.SetActive(true);
            }

            if (health <= 5)
            {
                fullHealth.SetActive(false);
                halfHealth.SetActive(true);
            }

            if (health == 0)
            {
                halfHealth.SetActive(false);
                noHealth.SetActive(true);
            }
        }
    }

    private void Awake()
    {
        instance = this;
    }


}
