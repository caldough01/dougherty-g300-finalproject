using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float knockbackPower = 20f;
    public float knockbackDuration = 0.5f;

    public PlayerController Player;

    public Animator anim;

    public Renderer sprite;
    public Collider2D collider1;
    public Collider2D collider2;
    public Collider2D collider3;


    public EnemyPatrol Patrolling;

    public AudioController Sounds; 

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player" && Player.isDashing == true)
        {
            


            Sounds.PlayDeathSound();

            Destroy(this.gameObject);
            

        }
        else if (other.gameObject.tag == "Player" && Player.isDashing == false)
        {
            StartCoroutine(EnemyHit.instance.Knockback(knockbackDuration, knockbackPower, this.transform));
        }
    }


    
}
