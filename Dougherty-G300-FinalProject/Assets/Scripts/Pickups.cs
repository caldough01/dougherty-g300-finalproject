using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pickups : MonoBehaviour
{
    


    public GameObject Key;

    public float keyCount = 0;


    private void Start()
    {


        Key.SetActive(false);
        
    }
    void Awake()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }
    void OnTriggerEnter2D(Collider2D key)
    {
        if (key.CompareTag("Player"))
        {
            keyCount = keyCount + 1;

            Destroy(gameObject);

            Key.SetActive(true);

        }
    }
}
