using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsController : MonoBehaviour
{
    public GameObject npcSaved;

    float npcPoints = 0;
    public Text points;




    public void AddPoints()
    {
        npcPoints = npcPoints + 1;
    }


    // Update is called once per frame
    void Update()
    {
        points.text = "You saved " + npcPoints + " out of 10 civilians";
    }
}
