using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cage : MonoBehaviour
{
    public Collider2D collision;
    public PlayerController Player;
    public PointsController PointsController;

    


    void Start()
    {
        
    }
    void Awake()
    {
        collision.isTrigger = true;
    }
    void OnTriggerEnter2D(Collider2D cage)
    {
        if (cage.CompareTag("Player") && Player.isDashing == true)
        {
            PointsController.AddPoints();
            
            

            Destroy(this.gameObject);

            
        }
    }

    void Update()
    {
        
    }
}
