using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public Rigidbody2D rB2D;

    public SpriteRenderer spriteRender;
    public Animator anim;
    public float runSpeed = 400;
    public float jumpSpeed;

    //Dashing
    public float dashDistance = 15f;
    public bool isDashing;
    float doubleTapTime;
    KeyCode lastKeyCode;
    
    public float dashCooldown = 10f;
    private bool cooldown;
    public GameObject dash;

    bool shouldJump;
    bool setJump;

    public GameObject instructions;
    public GameObject instructBackground;
    float fixedDeltaTime;

    public GameObject gameEnd;
    public GameObject door;

    //public EnemyHit GettingHit;


    void Awake()
    {
        this.fixedDeltaTime = Time.fixedDeltaTime;

        
    }

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();

        var dashRenderer = dash.GetComponent<Renderer>();
        dashRenderer.material.SetColor("_Color", Color.green);

        
        instructions.SetActive(true);
        instructBackground.SetActive(true);
        Time.timeScale = 0f;
    }

    public void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        /*if (GettingHit.isKnockbacked == true)
        {
            horizontalInput = 0;
        }
        else
        {
            horizontalInput = Input.GetAxis("Horizontal");
        } */

        if (!isDashing /*&& horizontalInput != 0 */)
        {
            rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);
        }
        

        if (shouldJump)
        {
            rB2D.velocity = new Vector2(rB2D.velocity.x, rB2D.velocity.y + jumpSpeed);

            anim.SetBool("isJumping", true);
            

            shouldJump = false;
        }
        else
        {
            anim.SetBool("isJumping", false);
            
        }

        if (rB2D.velocity.x >= 0)
        {
            spriteRender.flipX = false;
        }
        else
        {
            spriteRender.flipX = true;
        }

        if (Mathf.Abs(horizontalInput) > 0f && setJump == false)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }

    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            instructions.SetActive(false);
            instructBackground.SetActive(false);

            Time.timeScale = 1f;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                shouldJump = true;

                
            }
            

        }


        //Dashing Left
        if (Input.GetKeyDown(KeyCode.A) && cooldown == false)
        {
            if (doubleTapTime > Time.time && lastKeyCode == KeyCode.A)
            {
                StartCoroutine(Dash(-1f));
            }
            else
            {
                doubleTapTime = Time.time + 0.5f;
            }

            lastKeyCode = KeyCode.A;
        }

        //Dashing Right
        if (Input.GetKeyDown(KeyCode.D) && cooldown == false)
        {
            if (doubleTapTime > Time.time && lastKeyCode == KeyCode.D)
            {
                StartCoroutine(Dash(1f));
            }
            else
            {
                doubleTapTime = Time.time + 0.5f;
            }

            lastKeyCode = KeyCode.D;
        }

        //Dash Cooldown

        if (cooldown)
        {
            if (dashCooldown > 0)
            {
                dashCooldown -= Time.deltaTime;

                var dashRenderer = dash.GetComponent<Renderer>();
                dashRenderer.material.color = new Color(253, 0, 0);
            }
            else
            {
                cooldown = false;
                dashCooldown = 5;

                var dashRenderer = dash.GetComponent<Renderer>();
                dashRenderer.material.SetColor("_Color", Color.green);
            }
        }

        

    }

    IEnumerator Dash (float direction)
    {
        isDashing = true;
        rB2D.velocity = new Vector2(rB2D.velocity.x, 0f);
        rB2D.AddForce(new Vector2(dashDistance * direction, 0f), ForceMode2D.Impulse);
        float gravity = rB2D.gravityScale;
        rB2D.gravityScale = 0;
        yield return new WaitForSeconds(0.4f);
        isDashing = false;
        rB2D.gravityScale = 2;

        SetCooldownText();
        
    }



    void SetCooldownText()
    {
        cooldown = true;
    }
}
